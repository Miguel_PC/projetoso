https://www.thegeekstuff.com/2012/05/c-mutex-examples/

Para compilar linux abra o terminal do linux e digite:

gcc jobs.c –lpthread –o jobs.io

Para compilar macOS abra o terminal do mac e digite:

gcc jobs.c -o jobs.io

Para executar codigo digite:

./jobs.io

Para compilar linux  abra o terminal linux e digite:

gcc jobs_sem_lock.c –lpthread –o jobs_sem_lock.io

Para compilar macOS abra o terminal  mac e digite:

gcc jobs_sem_lock.c -o jobs_sem_lock.io

Para executar codigo digite:

./jobs_sem_lock.io


O codigo jobs.c possuia a variavel de lock para sincronização de threads, travando o recurso compartilhado ao entrar na regiao critica e destravando o recurso ao terminar a execução de um job(thread) partindo para o proximo.
O codigo jobs_sem_lock.c é o mesmo codigo apenas, sem a sincronização de threads, fazendo com que no meio do processamento de uma thread outra se inicie sem terminar a anterior.

Integrantes: Miguel Cunha