#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
//https://www.thegeekstuff.com/2012/05/c-mutex-examples/
// para compilar linux gcc jobs.c –lpthread –o jobs.io
// para compilar macOS gcc jobs.c -o jobs.io
// para executar codigo ./jobs.io
pthread_t tid[60]; //Declarada a quantidade de threads a serem executadas.
int counter; //Recurso a ser compartilhado entre as threads.
pthread_mutex_t lock;

void* doSomeThing(void *arg)//metodo que sera executado dentro de cada thread, utilizando o recurso compartilhado counter.
{
	//Um Job so pode ser iniciado se seu anterior tiver sido concluido.
	//O lock server para sincronizar as threads permitindo que um Job sempre seja aberto e termiando antes de executar o proximo Job.
    pthread_mutex_lock(&lock);//trava o codigo ao entrar na regiao critica

    counter += 1;
    printf("\n Job %d started\n", counter);

    sleep(1);

    printf("\n Job %d finished\n", counter);

    pthread_mutex_unlock(&lock);//Destrava a regiao critica e o recurso compartilhado para o proximo job a ser executado.

    return NULL;
}

int main(void)
{
    int i = 0;
    int err;

    if (pthread_mutex_init(&lock, NULL) != 0)//inicia a variavel de lock
    {
        printf("\n mutex init failed\n");
        return 1;
    }

    while(i < 60)
    {
        err = pthread_create(&(tid[i]), NULL, &doSomeThing, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
        i++;
    }

    
    pthread_join(tid[59], NULL);
    pthread_mutex_destroy(&lock); //destroi a variavel de lock

    return 0;
}